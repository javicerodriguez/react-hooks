import React from 'react';
import Characters from './components/Chraracters';
import Header from './components/Header';
import './App.css'

function App() {
  return (
    <div className='App'>
      <Header />
      <Characters />
    </div>
  );
}

export default App;
